from functools import wraps
from typing import List, Callable
import os
import time
import json
from inspect import getmembers, isfunction
import logging
from pathlib import Path

from setuptools.config import read_configuration
from git import Repo


def load_project_param(with_opts=False):
    """

    Examples:
      >>> p = load_project_param()
      >>> p['name']
      'proma'

    """
    conf_pth = "setup.cfg"
    conf_dict = read_configuration(conf_pth)

    param = {}
    param.update(**conf_dict["option"])
    param.update(**conf_dict["metadata"])
    if with_opts:
        param.update(**conf_dict["options"])

    param["script_name"] = "setup.py"

    return param


def init_proma_dir():
    os.makedirs(".proma", exist_ok=True)

    clean_pth = os.path.join(".proma", "clean.sh")
    if not os.path.exists(clean_pth):
        os.rename("clean.sh", clean_pth)

    param = load_project_param(with_opts=True)
    if "install_requires" in param.keys():
        deps = param["install_requires"]
    else:
        deps = []

    req_pth = os.path.join(".proma", "requirements.txt")
    f = open(req_pth, "w")
    for r in deps:
        f.write("%s\n" % r)
    f.close()


def init_git_repo():
    log = logging.getLogger("proma_logger")

    wd = Path(os.getcwd())

    # if os.path.exists('./.git'):
    #     log.error("The project's folder '%s' is already a git working directory" % wd)
    #     return False

    repo = Repo.init(".")

    repo.index.add("*")
    repo.index.add(".gitignore")
    repo.index.add(".gitlab-ci.yml")

    repo.index.commit("Project initialization")

    return True


def list_proma_commands():
    from proma import proma as p

    functions_list = [
        o[1]
        for o in getmembers(p)
        if isfunction(o[1]) and hasattr(o[1], "proma_command") and o[1].proma_command
    ]

    return functions_list


def get_status(func: Callable) -> int:
    status_pth = os.path.join(".proma", "status.json")

    try:
        json_file = open(status_pth, "r")
        data = json.load(json_file)
        json_file.close()
    except:
        data = {}

    for f in list_proma_commands():
        fn = f.__name__
        if not fn in data.keys():
            data[fn] = 0

    return data[func.__name__]


def update_status(func: Callable, t_ns: int = None) -> int:
    status_pth = os.path.join(".proma", "status.json")

    if t_ns is None:
        t_ns = time.time_ns()

    try:
        json_file = open(status_pth, "r")
        data = json.load(json_file)
        json_file.close()
    except:
        data = {}

    for f in list_proma_commands():
        fn = f.__name__
        if not fn in data.keys():
            data[fn] = 0

    data[func.__name__] = t_ns

    with open(status_pth, "w") as outfile:
        json.dump(data, outfile)

    return t_ns


def _walk_git_tree(tree, res=[], root="."):
    for f in tree:
        if type(f).__name__ == "Tree":
            res.extend(_walk_git_tree(f, res=res, root=os.path.join(root, f.name)))
        else:
            res.append(os.path.join(root, f.name))
    return set(res)


def list_project_files():
    try:
        repo = Repo.init(".")
        tree = repo.tree()
        res = _walk_git_tree(tree)
    except:
        # In case the git repo is not yet initialized
        res = []

    return res


def get_project_mod_time():
    log = logging.getLogger("proma_logger")

    files = list_project_files()
    mod_time_ns = 0
    for f in files:
        s = os.stat(f)
        mod_time_ns = max(mod_time_ns, s.st_mtime_ns)

    if mod_time_ns == 0:
        log.warning(
            "The project seems uninitialized (not a git working directory). Consider proma init"
        )

    return mod_time_ns


def requires(deps: List[Callable] = []):
    """
    Decorator which executes the functions of the list 'deps'
    before calling the decorated function

    """

    def require2(func):
        @wraps(func)
        def new_func(*args, **kwargs):
            mod_time = get_project_mod_time()
            for f in deps:
                f_time = get_status(f)
                if mod_time > f_time or mod_time == 0:
                    f(*args, **kwargs)

            msg = "Executing '%s'" % func.__name__
            print("=" * len(msg))
            print(msg)
            print("=" * len(msg))

            res = func(*args, **kwargs)
            if res:
                update_status(func)

            print()

        new_func.proma_command = True

        return new_func

    return require2
