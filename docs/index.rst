.. include:: ../README.rst

Content
=======

.. toctree::
   :maxdepth: 4

   modules
    
Indices and tables
==================

* :ref:`genindex`
